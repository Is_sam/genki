import { Injectable } from '@angular/core';

import { Room } from '../../models/room';

@Injectable()
export class Rooms {
  rooms: Room[] = [];

  defaultRoom: any = {
    "name": "Burt Bear Room",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "I have issues being the only bear in the jungle.",
  };


  constructor() {
    let rooms = [
      {
        "name": "Burt Bear",
        "profilePic": "assets/img/speakers/bear.jpg",
        "about": "Burt is a Bear."
      },
      {
        "name": "Charlie Cheetah",
        "profilePic": "assets/img/speakers/cheetah.jpg",
        "about": "Charlie is a Cheetah."
      },
      {
        "name": "Donald Duck",
        "profilePic": "assets/img/speakers/duck.jpg",
        "about": "Donald is a Duck."
      },
      {
        "name": "Eva Eagle",
        "profilePic": "assets/img/speakers/eagle.jpg",
        "about": "Eva is an Eagle."
      },
      {
        "name": "Ellie Elephant",
        "profilePic": "assets/img/speakers/elephant.jpg",
        "about": "Ellie is an Elephant."
      },
      {
        "name": "Molly Mouse",
        "profilePic": "assets/img/speakers/mouse.jpg",
        "about": "Molly is a Mouse."
      },
      {
        "name": "Paul Puppy",
        "profilePic": "assets/img/speakers/puppy.jpg",
        "about": "Paul is a Puppy."
      }
    ];

    for (let room of rooms) {
      this.rooms.push(new Room(room));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.rooms;
    }

    return this.rooms.filter((room) => {
      for (let key in params) {
        let field = room[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return room;
        } else if (field == params[key]) {
          return room;
        }
      }
      return null;
    });
  }

  add(room: Room) {
    this.rooms.push(room);
  }

  delete(room: Room) {
    this.rooms.splice(this.rooms.indexOf(room), 1);
  }
}
