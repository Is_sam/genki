/**
 * The Rooms service manages creating instances of Room.
 * 
 * A chat room 
 */
export class Room {

  constructor(fields: any) {
    // Quick and dirty extend/assign fields to this model
    for (const f in fields) {
      // @ts-ignore
      this[f] = fields[f];
    }
  }

}

export interface Room {
  [prop: string]: any;
}
