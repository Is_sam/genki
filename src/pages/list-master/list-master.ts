import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { Room } from '../../models/room';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {

  roomRsef: AngularFireList<any>;
  rooms: Observable<Room[]>;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public db: AngularFireDatabase) {
    this.roomRsef = db.list('rooms');
    this.rooms = this.roomRsef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  }

  /**
  * The view loaded, let's query our items for the list
  */
  ionViewDidLoad() {
  }

  /**
  * Prompt the user to add a new Room. This shows our RoomCreatePage in a
  * modal and then adds the new room to our data source if the user created one.
  */
  addRoom() {
    let addModal = this.modalCtrl.create('RoomCreatePage');
    addModal.onDidDismiss(room => {
      if (room) {
        this.roomRsef.push(room);
      }
    })
    addModal.present();
  }

  /**
  * Navigate to the detail page for this room.
  */
  openRoom(room: Room) {
    this.navCtrl.push('RoomDetailPage', {
      room: room
    });
  }

  /**
  * Navigate to the detail page for this room.
  */
  joinRoom(room: Room) {
    this.navCtrl.push('RoomChatPage', {
      room: room
    });
  }


  deleteRoom(room: Room) {
    this.roomRsef.remove(room.key);
  }

  update(room: any, params: any) {
    this.roomRsef.update(room.key, params);
  }
}