import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { RoomChatPage } from './room-chat';

@NgModule({
  declarations: [
    RoomChatPage,
  ],
  imports: [
    IonicPageModule.forChild(RoomChatPage),
    TranslateModule.forChild()
  ],
  exports: [
    RoomChatPage
  ]
})
export class RoomChatPageModule { }
