import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { Chat } from '../../models/chat';
import { Room } from '../../models/room';

@IonicPage()
@Component({
  selector: 'page-room-chat',
  templateUrl: 'room-chat.html'
})
export class RoomChatPage {
  data = { type: 'message', message: '' };

  chatRef: AngularFireList<any>;
  chats: Observable<Chat[]>;
  room: Room;

  nickname: string = 'John Papa';
  offStatus: boolean = false;

  constructor(public navCtrl: NavController, navParams: NavParams, public db: AngularFireDatabase) {
    this.room = navParams.get('room');

    this.chatRef = db.list('rooms/' + this.room.key + '/chats');
    this.chats = this.chatRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });

    this.joinMessage();
  }

  sendMessage(type?: string, message?: string) {
    let newData = {
      type: type ? type : this.data.type,
      user: this.nickname,
      message: message ? message : this.data.message,
      sendDate: Date()
    };
    this.chatRef.push(newData);
    this.data.message = '';
  }

  joinMessage(type?: string, message?: string) {
    let newData = {
      type: 'join',
      user: this.nickname,
      message: this.nickname + ' has joined the room',
      sendDate: Date()
    };
    this.chatRef.push(newData);
    this.data.message = '';
  }
}
