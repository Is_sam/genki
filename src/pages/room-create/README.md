# Room Create

The Room Create Page creates new instances of `Room`, and will most commonly be used in a modal window to be presented by `ListMasterPage`.
