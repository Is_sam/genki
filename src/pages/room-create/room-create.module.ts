import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { RoomCreatePage } from './room-create';

@NgModule({
  declarations: [
    RoomCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(RoomCreatePage),
    TranslateModule.forChild()
  ],
  exports: [
    RoomCreatePage
  ]
})
export class RoomCreatePageModule { }
