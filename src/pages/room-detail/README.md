# Item Detail

The Room Detail Page shows the details of instances of a chat Room, and will most commonly be navigated to from `ListMasterPage`.
