import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { RoomDetailPage } from './room-detail';

@NgModule({
  declarations: [
    RoomDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RoomDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    RoomDetailPage
  ]
})
export class RoomDetailPageModule { }
