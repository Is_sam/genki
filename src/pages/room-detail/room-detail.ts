import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-room-detail',
  templateUrl: 'room-detail.html'
})
export class RoomDetailPage {
  room: any;

  constructor(public navCtrl: NavController, navParams: NavParams) {
    this.room = navParams.get('room');
  }

}
